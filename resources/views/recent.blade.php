<div class="inner cover">
	<h1 class="cover-heading">Recent Flickr Photos</h1>
	<div style="width: 830px;">
		@foreach ($recentPhotos as $photo)
		<a href="/sizes/{{ $photo['id'] }}">
			<div class="flickr-tiny-photo">
				<img src="https://farm{{ $photo['farm'] }}.staticflickr.com/{{ $photo['server'] }}/{{ $photo['id'] }}_{{ $photo['secret'] }}_q.jpg"/>
			</div>
		</a>
		@endforeach
	</div>
</div>