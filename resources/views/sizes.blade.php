<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Flickr NIX Viewer</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="/css/cover.css" rel="stylesheet">
  </head>

  <body>
    <div class="site-wrapper">
      <div class="site-wrapper-inner">
        <div class="cover-container">
          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand"><a href="/">Flickr NIX Viewer</a></h3>
              <nav>
                <ul class="nav masthead-nav">
                  <li class="active"><a href="/">Home</a></li>
                  <li><a href="#">Contact</a></li>
                </ul>
              </nav>
            </div>
          </div>

            <div class="inner cover main-view">
                <h1 class="cover-heading">Pick size</h1>
                <div style="width: 830px;">
                    <div>
                        @foreach ($sizes as $size)
                            <div style="float: left; padding: 5px;">
                                <a type="button" class="btn btn-default" href="{{ $size['source'] }}">{{ $size['label'] }} ({{ $size['width'] }}x{{ $size['height'] }})</a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

          <div class="mastfoot">
            <div class="inner">
              <p>Flickr NIX Viewer, by Olya Dmytriv.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
