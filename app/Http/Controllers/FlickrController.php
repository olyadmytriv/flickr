<?php

namespace App\Http\Controllers;

use App\libraries\phpflickr\phpFlickr;

class FlickrController extends Controller
{
	private $phpFlickr;
	
	public function __construct() {
		$this->phpFlickr = new phpFlickr("a75a700c9bcb5b97df430af83cf86c28");
	}
	
	public function index()
	{
		return view('index');
	}
	
	public function recent()
	{
		$recentPhotos = $this->phpFlickr->photos_getRecent(NULL, NULL, 20, 1);

		return view('recent')
			->with('recentPhotos', $recentPhotos['photos']['photo']);
	}

	public function sizes($photoId)
	{
		$sizes = $this->phpFlickr->photos_getSizes($photoId);
		
		return view('sizes')
			->with('sizes', $sizes);
	}
}
