## Run application

php artisan serve

## Used libraries

- Laravel 5.3
- AngularJS 1.6.1
- Bootstrap: http://getbootstrap.com/
- phpFlickr: https://github.com/dan-coulter/phpflickr

## Screenshots

![Screen Shot 2017-02-05 at 10.21.23 PM.png](https://bitbucket.org/repo/px44q5/images/740285700-Screen%20Shot%202017-02-05%20at%2010.21.23%20PM.png)

![Screen Shot 2017-02-05 at 10.21.51 PM.png](https://bitbucket.org/repo/px44q5/images/2606140925-Screen%20Shot%202017-02-05%20at%2010.21.51%20PM.png)